# Holomic Spatialverse #



### What is Holomic Spatialverse? ###

**Holomic Spatialverse** is an ML/AI platform that provides predictive analysis and visualizations of spatially resolved biological data.